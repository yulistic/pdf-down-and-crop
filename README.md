Author: yulistic (Jongyul Kim)

This is README file for `pdf-down-and-crop` script.

# Prerequisites
`wget` and `pdfcrop` should be available.

# Usage
```
pdc.sh <url>
```
It will download a file from the `url` and crop it.  
You can adjust margins of the output file with `pdfcrop` option: `--margins "<left> <top> <right> <bottom>"`  (Ex> "8 8 8 8" please refer to `pdfcrop --help`).
